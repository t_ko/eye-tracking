clear all;
close all;
load('train.mat');


subjects = { 's5', 's15', 's25', 's1', 's11', 's21' };

% number of observations for each subject per image
s5Obs = 0;
s15Obs = 0;
s25Obs = 0;
s1Obs = 0;
s11Obs = 0;
s21Obs = 0;

for i = 1 : length(sid)

  % subject s5
  if (strcmp(sid(i), subjects(1)))

    s5Obs = s5Obs + 1;
    s5Centroids(:, :, s5Obs) = mean(coordinates{i});


  % subject s15
  elseif (strcmp(sid(i), subjects(2)))

    s15Obs = s15Obs + 1;
    s15Centroids(:, :, s15Obs) = mean(coordinates{i});

  % subject s25
  elseif (strcmp(sid(i), subjects(3)))

    s25Obs = s25Obs + 1;
    s25Centroids(:, :, s25Obs) = mean(coordinates{i});

    % subject s1
  elseif (strcmp(sid(i), subjects(4)))

    s1Obs = s1Obs + 1;
    s1Centroids(:, :, s1Obs) = mean(coordinates{i});

  % subject s11
  elseif (strcmp(sid(i), subjects(5)))

    s11Obs = s11Obs + 1;
    s11Centroids(:, :, s11Obs) = mean(coordinates{i});

  % subject s21
  elseif (strcmp(sid(i), subjects(6)))

    s21Obs = s21Obs + 1;
    s21Centroids(:, :, s21Obs) = mean(coordinates{i});

  end 
end
